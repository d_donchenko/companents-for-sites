// work with URL of the site

function getUrlParameter(param) {
    /*
    * search GET parameter in site-URL. return GET parameter value or false if didn't find it
    * поиск GET-параметра в URL сайта. возвращает значение GET-параметра или false если нет такого параметра
    */
    var pageURL = window.location.search.substring(1),
        urlVariables = pageURL.split('&'),
        parameterName;

    for (var i = 0; i < urlVariables.length; i++) {
        parameterName = urlVariables[i].split('=');
        
        if (parameterName[0] === param) {
            return parameterName[1] === undefined ? true : decodeURIComponent(parameterName[1]);
        }
    }
    return false;
}

function updateUrl(data) {
    /*
    * We work with an object that stores the names and values of the applied filters. Add them to the address line
    * Работаем с объектом хранящим в себе имена и значения применяемых фильтров. Добавляем их в адрессную строку
    */
    let searchParams = new URLSearchParams(window.location.search);

    for (let key in data)
        searchParams.set(key, data[key]);

    let newRelativePathQuery = window.location.pathname + '?' + searchParams.toString();
    history.pushState(null, '', newRelativePathQuery);
    let newUrl = window.location.pathname.replace(/\/\d*?$/, '') + window.location.search;
    window.history.pushState('', '', newUrl);
}

function checkGetParams(params) {
    /*
    * We check GET-parameters whose names are passed to this function, and return an object that stores the names and values of GET-parameters or null if GET-parameters didn't find
    * Проверяем GET-параметры, имена которых переданы в эту функцию, и возвращаем объект хранящий в себе имена и значения GET параметров или null если GET-параметры не найдены
    * */
    let result = {};

    params.forEach(function (param) {
        if (getUrlParameter(param)) {
            result[param] = getUrlParameter(param);
            $('.filter select[name=' + param + '] option[value="' + getUrlParameter(param) + '"]').attr('selected', 'selected')
        }
    });
    if (Object.keys(result).length > 0)
        return result;
    else
        return null
}

// end work with URL of the site

function fillTemplate(data, template, replase) {
    /*
    * fill in the template with the transferred data
    * data - data which we will fill
    * template - template to be filled
    * replase - an object with two necessary keys: replacesedStreengs(strings to be replaced in the template) and replacesedKeys(keys in the data variable, to which values will be replaced or strings to be replaced)
    * IMPORTANT!!! keep order when writing replacedStrings and replacedKeys
    *
    * заполняем переданный шаблон переданными данными
    * data - данные на которые будет произведена замена
    * template - шаблон в котором будет производится замена
    * replase - обхект который должен иметь два ключа:  replacesedStreengs(строки которые будут заменяться) и replacesedKeys(ключи с переменной data по которым будет производится замена или уникальные строки на которые будет произведена замена)
    * ВАЖНО!!! сохраняйте порядок при написании replacedStrings и replacedKeys
    */
    let fill = '';

    var replasedStreengs = replase.replacedStrings
    var replasedKeys = replase.replacedKeys

    if (template) {
        data.forEach(function (item) {
            let tmp = template;
            for (var key = 0; key < replasedStreengs.length; key++) {
                var str1 = replasedStreengs[key];
                var str2 = item.hasOwnProperty(replasedKeys[key]) ? item[replasedKeys[key]].toString() : replasedKeys[key];
                tmp = tmp.replace(str1, str2)
            }
            fill += tmp
        });
    } else
        fill += 'invalid template, check template!!!'

    return fill
}

function get(insertSelector, url, data, template, replase, message = 'Ничего не найдено') {
    /*
    * get data from server
    * insertSelector - HTML class or id for insert processed template
    * data - data for ajax query
    * template - template to be filled
    * replase - an object with two necessary keys: replacesedStreengs(strings to be replaced in the template) and replacesedKeys(keys in the data variable, to which values will be replaced or strings to be replaced)
    * IMPORTANT!!! keep order when writing replacesedStreengs and replacesedKeys
    *
    * получение данных с сервера
    * insertSelector - HTML класс или id по которому будет вставлен обработаный шаблон
    * data - данные для ajax-запроса
    * template - шаблон в котором будет производится замена
    * replase - обхект который должен иметь два ключа:  replacesedStreengs(строки которые будут заменяться) и replacesedKeys(ключи с переменной data по которым будет производится замена или уникальные строки на которые будет произведена замена)
    * ВАЖНО!!! сохраняйте порядок при написании replacesedStreengs и replacesedKeys
    */

    $(insertSelector).addClass('load');

    $.ajax({
        url: url,
        data: data,
        dataType: 'json'
    }).done(function (response) {
        let insertHtml = '';
        if (response.length > 0)
            insertHtml += fillTemplate(response, template, replase);
        else
            insertHtml += `<p>${message}</p>`;
        $(insertSelector).html(insertHtml).removeClass('load')
    });
}