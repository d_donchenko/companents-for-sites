function filter(option) {
    $(option.selector).on('click', function () {
        let filterValue = $(this).data('filter');
        $(this).parent().addClass('active').siblings().removeClass('active');
        if(filterValue !== '*'){
            $(option.gridSelector).each(function () {
                if(!$(this).hasClass(filterValue))
                    $(this).addClass('hide').removeClass('show');
                else
                    $(this).removeClass('hide').addClass('show');
            })
        } else{
            $(option.gridSelector).removeClass('hide').addClass('show');
            setTimeout(function () {
                $(option.gridSelector).removeClass('show');
            }, 1000)
        }
    });
}