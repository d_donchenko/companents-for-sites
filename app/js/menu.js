$(document).ready(function () {
    $('button.show-menu').on('click', function () {
        $(this).siblings('ul.menu').slideToggle()
    });
    if($(window).width() < 991){
        $('.menu-item-has-children').on('click', function (e) {
            if(e.target === this){
                $(this).children('.sub-menu').toggleClass('show');
                $(this).siblings('.menu-item-has-children').children('.sub-menu').removeClass('show');
            }
        })
    }
});