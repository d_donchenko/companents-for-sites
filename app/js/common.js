$(document).ready(function () {
    if(typeof(checkGetParams) === 'function'){
        let paramsName = ['test', 'userId'];
        let getParams = checkGetParams(paramsName);
        let template = $('#template').html();
        let replace = {
            "replacedStrings": [
                "@id@",
                "data-src",
                "@img@",
                "@head@",
                "@content@"
            ],
            "replacedKeys": [
                "userId",
                "src",
                "https://via.placeholder.com/540",
                "title",
                "body"
            ]
        };

        if (getParams)
            get('.get_content', 'https://jsonplaceholder.typicode.com/posts', getParams, template, replace);

        $('.send_request').on('click', function () {
            let data = {};
            let urlParams = {};
            let template = $('#' + $(this).data('template')).html();

            $('.filter select').each(function () {
                if ($(this).val() !== null && $(this).val() !== '') {
                    data[$(this).attr('name')] = $(this).val();
                    urlParams[$(this).attr('name')] = $(this).val()
                }
            });

            updateUrl(urlParams);
            get('.get_content', 'https://jsonplaceholder.typicode.com/posts', data, template, replace)
        });
    }

    if(typeof(filter) === 'function'){
        let filterSelector = $('.grid-filter-buttons button');
        let filterGrid = $('.filter-grid > div');
        filter({
            selector: filterSelector,
            gridSelector: filterGrid
        });
    }


});
