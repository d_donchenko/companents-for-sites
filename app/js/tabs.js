$(document).ready(function () {
    $('.tabs-head').on('click', function () {
        let tab = $(this).data('tab-activate')
        $('.tabs-head.active').removeClass('active')
        $('.tabs-head').each(function () {
            if( $(this).data('tab-activate') === tab )
                $(this).addClass('active')
        })
        $('.tabs-body').each(function () {
            if( $(this).data('tab-activate') === tab ) {
                $('.tabs-body').slideUp()
                $(this).slideDown()
            }
        })
    })
});