$(document).ready(function () {
    $('*[data-modal]').on('click', function () {
        $('body').addClass('modal-open');
        var modal = $(this).data('modal');
        $('.' + modal).fadeIn();
    });
    $('.modal-container .modal-close').on('click', function () {
        $('.modal-container').fadeOut()
        $('body').removeClass('modal-open')
    });
    $('.modal-container .modal-wrap').on('click', function (e) {
        if (e.target !== this)
            return;
        else{
            $('.modal-container').fadeOut()
            $('body').removeClass('modal-open')
        }
    })
});



(function($){
    $.fn.closeModal = function(){
        $(this).fadeOut();
        $('html').removeClass('modal-open');
    };
    $.fn.openModal = function(){
        $(this).fadeIn();
        $('html').addClass('modal-open');
    };
})(jQuery);