$(document).ready(function () {
    $('button[data-accordion-activate]').on('click', function () {
        var toActivate = $(this).data('accordion-activate');

        var head = $(this).parent('.accordion-head');
        var body = $('.accordion-body');

        if(head.hasClass('active')){
            head.removeClass('active');
            body.slideUp()
        }
        else{
            $('.accordion-head').removeClass('active');
            head.addClass('active');
            body.slideUp()
                .each(function () {
                    if($(this).data('accordion-activate') === toActivate)
                        $(this).slideDown()
                })
        }
    })
});
